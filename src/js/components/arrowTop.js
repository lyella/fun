/**
 * Moves the page to the top using arrow
 */

const arrowTop = () => {
  const scrollToTopBtn = document.querySelector('.footer__top')
  const rootElement = document.documentElement
  const TOGGLE_RATIO = 0.10;

  const handleScroll = () => {
    const scrollTotal = rootElement.scrollHeight - rootElement.clientHeight

    if ((rootElement.scrollTop / scrollTotal) > TOGGLE_RATIO) {
      scrollToTopBtn.classList.add('footer__show')
    } else {
      scrollToTopBtn.classList.remove('footer__show')
    }
  }

  const scrollToTop = () => {
    rootElement.scrollTo({
      top: 0,
      behavior: 'smooth'
    })
  }

  scrollToTopBtn.addEventListener('click', scrollToTop)
  document.addEventListener('scroll', handleScroll)
}

export default arrowTop
