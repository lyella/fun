/**
 * Get zip using ajaxzip3
 */

const getZip = () => {
  const getZip = document.querySelector('.contact__address');

  if (getZip) {
    getZip.addEventListener('click', event => {
      AjaxZip3.zip2addr("zip", "","location", "address1", "address2");
      
      return false;
    });
  }
  else {
    console.log("This element doesn't exist.");
  }
}

export default getZip
