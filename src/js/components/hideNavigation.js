/**
 * Move the nav__links on the backwards first
 */

const hideNavigation = () => {
  const links = document.getElementsByClassName('nav__links');
  const btn = document.getElementsByClassName('header__btn');
  const container = document.getElementsByClassName('container');

  btn.onclick = () => {
    links.style.zIndex = links.style.zIndex === '2' ? '' : '2';
    container.style.zIndex = container.style.zIndex === '1' ? '' : '1';
  }
}

export default hideNavigation
