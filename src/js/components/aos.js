/**
 * Add animation on scroll using AOS by https://michalsnik.github.io/aos/
 */

const aos = () => {
  AOS.init({
    offset: 120,
    delay: 0,
    duration: 500,
    easing: 'ease',
    once: true,
    mirror: false
  });
}

export default aos