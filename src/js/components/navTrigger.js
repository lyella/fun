/**
 * Toggles the navigation on click
 */

const navTrigger = () => {
  const navTrigger = document.querySelector('.nav__btn'); 

  navTrigger.addEventListener('click', event => {
    if (window.innerWidth <= 767) {
      document.body.classList.toggle('overflow'); 
    }
  });
}

export default navTrigger
