/**
 * Toggles the dropdown menu on click
 */

const dropdownToggle = () => {
  const dropdownToggle = document.querySelector('.nav__dropdown-toggle');

  dropdownToggle.addEventListener('click', event => {
    dropdownToggle.classList.toggle('is-active');
  });
}

export default dropdownToggle
