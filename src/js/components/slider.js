/**
 * Flickity properties
 */

const slider = () => {
  const elem = document.querySelector('.slider');

  if(elem) {
    new Flickity( elem, {
      cellAlign: 'left',
      draggable: true,
      contain: true,
      wrapAround: true,
      prevNextButtons: true,
      autoPlay: 2000
    });
  }
  else {
    console.log("This element doesn't exist.");
  }
}

export default slider