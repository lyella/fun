import navTrigger from './components/navTrigger'
import dropdownToggle from './components/dropdownToggle'
import slider from './components/slider'
import hideNavigation from './components/hideNavigation'
import arrowTop from './components/arrowTop'
import getZip from './components/getZip'
import aos from './components/aos'

document.addEventListener(
  'DOMContentLoaded',
  () => {
    navTrigger()
    dropdownToggle()
    slider()
    hideNavigation()
    arrowTop()
    getZip()
    aos()
  },
  false
)
