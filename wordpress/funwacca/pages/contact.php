<?php /* Template Name: Contact */
get_header() ?>
  <div class="container">
    <div class="contact">
      <div class="contact__breadcrumbs" data-aos="flip-left"><a class="contact__breadcrumbs-text"
          href="#">トップページ</a><span class="contact__arrow-right">&#62;</span><a
          class="contact__breadcrumbs-text contact__breadcrumbs-text--current" href="#">お問い合わせ</a></div>
      <div class="container__content">
        <div class="title" data-aos="zoom-in">
          <div class="title__lines">
            <h1 class="title__text">CONTACT</h1>
          </div>
        </div>
      </div>
      <?php echo do_shortcode('[mwform_formkey key="30"]') ?>
    </div>
  </div>
<?php get_footer() ?>