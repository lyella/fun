<?php /* Template Name: Top */
get_header() ?>
<div class="container">
    <div class="slider" data-aos="fade-down">
      <div class="slider__cell"><img class="slider__img" src="<?php echo get_template_directory_uri() ?>/img/img__slider.jpg" alt="banner1"><img
          class="slider__img--sp" src="<?php echo get_template_directory_uri() ?>/img/img__slider-sp.jpg" alt="banner1"></div>
      <div class="slider__cell"><img class="slider__img" src="<?php echo get_template_directory_uri() ?>/img/img__slider.jpg" alt="banner2"><img
          class="slider__img--sp" src="<?php echo get_template_directory_uri() ?>/img/img__slider-sp.jpg" alt="banner2"></div>
      <div class="slider__cell"><img class="slider__img" src="<?php echo get_template_directory_uri() ?>/img/img__slider.jpg" alt="banner3"><img
          class="slider__img--sp" src="<?php echo get_template_directory_uri() ?>/img/img__slider-sp.jpg" alt="banner3"></div>
    </div>
    <div class="content">
      <section class="pickup">
        <div class="container__content">
          <div class="title" data-aos="zoom-in">
            <div class="title__lines">
              <h1 class="title__text">PICKUP</h1>
            </div>
          </div>
          <div class="content__item-container"><a class="content__item" href="#" data-aos="zoom-out"><img
                class="content__image" src="<?php echo get_template_directory_uri() ?>/img/pickup__img1.jpg" alt="pickup1">
              <div class="content__wrapper">
                <div class="content__date">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper">
                  <div class="content__category-line">
                    <p class="content__category-text content__category-text--skyblue">講習会</p>
                  </div>
                  <div class="content__title-line">
                    <p class="content__title">リアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item" href="#" data-aos="zoom-out"><img class="content__image"
                src="<?php echo get_template_directory_uri() ?>/img/pickup__img2.jpg" alt="pickup2">
              <div class="content__wrapper">
                <div class="content__date">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper">
                  <div class="content__category-line">
                    <p class="content__category-text content__category-text--islamicgreen">イベント</p>
                  </div>
                  <div class="content__title-line">
                    <p class="content__title">リアルをコアとした体験をデザインし、情報拡散や態度</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item" href="#" data-aos="zoom-out"><img class="content__image"
                src="<?php echo get_template_directory_uri() ?>/img/pickup__img3.jpg" alt="pickup3">
              <div class="content__wrapper">
                <div class="content__date">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper">
                  <div class="content__category-line">
                    <p class="content__category-text content__category-text--tawny">お知らせ</p>
                  </div>
                  <div class="content__title-line">
                    <p class="content__title">リアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証リアル...を</p>
                  </div>
                </div>
              </div>
            </a></div>
        </div>
      </section>
      <section class="update">
        <div class="container__content">
          <div class="title" data-aos="zoom-in">
            <div class="title__lines">
              <h1 class="title__text">WHAT'S NEW</h1>
            </div>
          </div>
        </div>
        <div class="update__container">
          <div class="content__item-container content__item-container--small"><a
              class="content__item content__item--small" href="#" data-aos="zoom-out"><img
                class="content__image content__image--small" src="<?php echo get_template_directory_uri() ?>/img/update__img1.jpg" alt="update1">
              <div class="content__wrapper content__wrapper--update">
                <div class="content__date content__date--small">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper content__category-wrapper--update">
                  <div class="content__category-line content__category-line--small">
                    <p class="content__category-text content__category-text--skyblue content__category-text--small">講習会
                    </p>
                  </div>
                  <div class="content__title-line content__title-line--small">
                    <p class="content__title content__title--small">リアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item content__item--small" href="#" data-aos="zoom-out"><img
                class="content__image content__image--small" src="<?php echo get_template_directory_uri() ?>/img/update__img2.jpg" alt="update2">
              <div class="content__wrapper content__wrapper--update">
                <div class="content__date content__date--small">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper content__category-wrapper--update">
                  <div class="content__category-line content__category-line--small">
                    <p
                      class="content__category-text content__category-text--islamicgreen content__category-text--small">
                      イベント</p>
                  </div>
                  <div class="content__title-line content__title-line--small">
                    <p class="content__title content__title--small">リアルをコアとした体験をデザインし、情報拡散や態度</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item content__item--small" href="#" data-aos="zoom-out"><img
                class="content__image content__image--small" src="<?php echo get_template_directory_uri() ?>/img/update__img3.jpg" alt="update3">
              <div class="content__wrapper content__wrapper--update">
                <div class="content__date content__date--small">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper content__category-wrapper--update">
                  <div class="content__category-line content__category-line--small">
                    <p class="content__category-text content__category-text--tawny content__category-text--small">お知らせ
                    </p>
                  </div>
                  <div class="content__title-line content__title-line--small">
                    <p class="content__title content__title--small">リアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証リアル...を</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item content__item--small" href="#" data-aos="zoom-out"><img
                class="content__image content__image--small" src="<?php echo get_template_directory_uri() ?>/img/update__img4.jpg" alt="update4">
              <div class="content__wrapper content__wrapper--update">
                <div class="content__date content__date--small">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper content__category-wrapper--update">
                  <div class="content__category-line content__category-line--small">
                    <p class="content__category-text content__category-text--amaranth content__category-text--small">募集
                    </p>
                  </div>
                  <div class="content__title-line content__title-line--small">
                    <p class="content__title content__title--small">リアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item content__item--small" href="#" data-aos="zoom-out"><img
                class="content__image content__image--small" src="<?php echo get_template_directory_uri() ?>/img/update__img5.jpg" alt="update5">
              <div class="content__wrapper content__wrapper--update">
                <div class="content__date content__date--small">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper content__category-wrapper--update">
                  <div class="content__category-line content__category-line--small">
                    <p class="content__category-text content__category-text--skyblue content__category-text--small">講習会
                    </p>
                  </div>
                  <div class="content__title-line content__title-line--small">
                    <p class="content__title content__title--small">リアルをコアとした体験をデザインし、情報拡散や態度</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item content__item--small" href="#" data-aos="zoom-out"><img
                class="content__image content__image--small" src="<?php echo get_template_directory_uri() ?>/img/update__img6.jpg" alt="update6">
              <div class="content__wrapper content__wrapper--update">
                <div class="content__date content__date--small">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper content__category-wrapper--update">
                  <div class="content__category-line content__category-line--small">
                    <p
                      class="content__category-text content__category-text--islamicgreen content__category-text--small">
                      イベント</p>
                  </div>
                  <div class="content__title-line content__title-line--small">
                    <p class="content__title content__title--small">リアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証リアル...を</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item content__item--small" href="#" data-aos="zoom-out"><img
                class="content__image content__image--small" src="<?php echo get_template_directory_uri() ?>/img/update__img1.jpg" alt="update1">
              <div class="content__wrapper content__wrapper--update">
                <div class="content__date content__date--small">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper content__category-wrapper--update">
                  <div class="content__category-line content__category-line--small">
                    <p class="content__category-text content__category-text--skyblue content__category-text--small">講習会
                    </p>
                  </div>
                  <div class="content__title-line content__title-line--small">
                    <p class="content__title content__title--small">リアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item content__item--small" href="#" data-aos="zoom-out"><img
                class="content__image content__image--small" src="<?php echo get_template_directory_uri() ?>/img/update__img2.jpg" alt="update2">
              <div class="content__wrapper content__wrapper--update">
                <div class="content__date content__date--small">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper content__category-wrapper--update">
                  <div class="content__category-line content__category-line--small">
                    <p
                      class="content__category-text content__category-text--islamicgreen content__category-text--small">
                      イベント</p>
                  </div>
                  <div class="content__title-line content__title-line--small">
                    <p class="content__title content__title--small">リアルをコアとした体験をデザインし、情報拡散や態度</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item content__item--small" href="#" data-aos="zoom-out"><img
                class="content__image content__image--small" src="<?php echo get_template_directory_uri() ?>/img/update__img3.jpg" alt="update3">
              <div class="content__wrapper content__wrapper--update">
                <div class="content__date content__date--small">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper content__category-wrapper--update">
                  <div class="content__category-line content__category-line--small">
                    <p class="content__category-text content__category-text--tawny content__category-text--small">お知らせ
                    </p>
                  </div>
                  <div class="content__title-line content__title-line--small">
                    <p class="content__title content__title--small">リアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証リアル...を</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item content__item--small" href="#" data-aos="zoom-out"><img
                class="content__image content__image--small" src="<?php echo get_template_directory_uri() ?>/img/update__img4.jpg" alt="update4">
              <div class="content__wrapper content__wrapper--update">
                <div class="content__date content__date--small">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper content__category-wrapper--update">
                  <div class="content__category-line content__category-line--small">
                    <p class="content__category-text content__category-text--amaranth content__category-text--small">募集
                    </p>
                  </div>
                  <div class="content__title-line content__title-line--small">
                    <p class="content__title content__title--small">リアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item content__item--small" href="#" data-aos="zoom-out"><img
                class="content__image content__image--small" src="<?php echo get_template_directory_uri() ?>/img/update__img5.jpg" alt="update5">
              <div class="content__wrapper content__wrapper--update">
                <div class="content__date content__date--small">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper content__category-wrapper--update">
                  <div class="content__category-line content__category-line--small">
                    <p class="content__category-text content__category-text--skyblue content__category-text--small">講習会
                    </p>
                  </div>
                  <div class="content__title-line content__title-line--small">
                    <p class="content__title content__title--small">リアルをコアとした体験をデザインし、情報拡散や態度</p>
                  </div>
                </div>
              </div>
            </a><a class="content__item content__item--small" href="#" data-aos="zoom-out"><img
                class="content__image content__image--small" src="<?php echo get_template_directory_uri() ?>/img/update__img6.jpg" alt="update6">
              <div class="content__wrapper content__wrapper--update">
                <div class="content__date content__date--small">
                  <p class="content__date-text">22 Jun, 2020</p>
                </div>
                <div class="content__category-wrapper content__category-wrapper--update">
                  <div class="content__category-line content__category-line--small">
                    <p
                      class="content__category-text content__category-text--islamicgreen content__category-text--small">
                      イベント</p>
                  </div>
                  <div class="content__title-line content__title-line--small">
                    <p class="content__title content__title--small">リアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証リアル...を</p>
                  </div>
                </div>
              </div>
            </a></div>
          <div class="sidepanel">
            <h1 class="sidepanel__title" data-aos="zoom-out">編集部からの⼀⾔</h1><img class="sidepanel__image"
              src="<?php echo get_template_directory_uri() ?>/img/sidepanel__img.jpg" alt="sidepanel image">
            <div class="sidepanel__content" data-aos="fade-down">
              <p class="content__title content__title--sidepanel">新型コロナウイルス緊急事態宣言解除後の当社対応についてのお知らせ</p>
              <p class="sidepanel__date">2020年7月22日</p>
              <p class="sidepanel__text">染まってないからこそ、新たな風を吹き込めるEVENT SOLUTION 特設サイトを公開リアルをコアと…</p>
            </div>
            <h1 class="sidepanel__title" data-aos="zoom-out">CATEGORY</h1>
            <div class="sidepanel__category-wrapper" data-aos="fade-down">
              <p class="content__category-text content__category-text--islamicgreen content__category-text--sidepanel">
                <a href="#">イベント</a></p>
              <p class="content__category-text content__category-text--skyblue content__category-text--sidepanel"><a
                  href="#">講習会</a></p>
              <p class="content__category-text content__category-text--tawny content__category-text--sidepanel"><a
                  href="#">お知らせ</a></p>
              <p class="content__category-text content__category-text--purple content__category-text--sidepanel"><a
                  href="#">コラム</a></p>
              <p class="content__category-text content__category-text--amaranth content__category-text--sidepanel"><a
                  href="#">募集</a></p>
            </div>
            <h1 class="sidepanel__title" data-aos="zoom-out">RANKING</h1>
            <div class="sidepanel__content" data-aos="fade-down">
              <ol class="sidepanel__ranking">
                <li class="sidepanel__ranking sidepanel__ranking--item"><a href="#">新型コロナウイルス緊急事態宣言解除後の当社対応についてのお知らせ</a>
                </li>
                <li class="sidepanel__ranking sidepanel__ranking--item"><a href="#">染まってないからこそ、新たな風を吹き込める</a></li>
                <li class="sidepanel__ranking sidepanel__ranking--item"><a href="#">EVENT SOLUTION 特設サイトを公開</a></li>
                <li class="sidepanel__ranking sidepanel__ranking--item"><a
                    href="#">リアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証</a></li>
                <li class="sidepanel__ranking sidepanel__ranking--item"><a
                    href="#">さまざまな業種や形態の体験施策に携わってきた経験値を武器に困難な状況を乗り越え</a></li>
              </ol>
            </div>
            <h1 class="sidepanel__title" data-aos="zoom-out">TAGS</h1>
            <div class="sidepanel__tags-wrapper" data-aos="fade-down"><a class="sidepanel__tags" href="#">コロナウイルス</a><a
                class="sidepanel__tags" href="#">イベント</a><a class="sidepanel__tags" href="#">地域活性化</a><a
                class="sidepanel__tags" href="#">サポーター</a><a class="sidepanel__tags" href="#">企画案</a><a
                class="sidepanel__tags" href="#">楽しむ</a><a class="sidepanel__tags" href="#">公園</a><a
                class="sidepanel__tags" href="#">フリマ</a><a class="sidepanel__tags" href="#">街づくり</a><a
                class="sidepanel__tags" href="#">つながり</a></div>
          </div>
        </div>
      </section>
    </div>
    <section class="banner" data-aos="zoom-in">
      <div class="banner__container"><img class="banner__image" src="<?php echo get_template_directory_uri() ?>/img/banner__img1.jpg" alt="Banner 1"><img
          class="banner__image banner__image--sp" src="<?php echo get_template_directory_uri() ?>/img/banner__img1-sp.jpg" alt="Banner 1">
        <div class="banner__wrapper">
          <div class="banner__text-container">
            <h1 class="banner__text banner__text--small">イベント企画‧まちづくりの⽅へ</h1>
            <div class="banner__text-wrapper banner__text-wrapper--small">
              <h1 class="banner__text">ファンワッカが<br class="banner__break">お⼿伝いします！</h1>
            </div>
            <p class="banner__text banner__text--description">これはダミー文字です新型コロナウイルス緊急事態宣言解除後の当社対応についてのお知らせ <br
                class="banner__break">染まってないからこそ、新たな風を吹き込めるリアルをコアとした体験を</p>
          </div>
          <div class="banner__button-container"><a class="button" href="#">詳しく見る</a></div>
        </div>
      </div>
    </section>
    <div class="content">
      <section class="category">
        <div class="container__content">
          <div class="title" data-aos="zoom-in">
            <div class="title__lines">
              <h1 class="title__text">CATEGORYS</h1>
            </div>
          </div>
          <div class="category__container">
            <div class="category__content">
              <div class="category__item-container"><a class="category__item" href="#">
                  <div class="category__title-container" data-aos="flip-up">
                    <p class="category__title-jp category__title-jp--islamicgreen">イベント</p>
                    <p class="category__title-en">EVENT</p>
                  </div>
                </a></div>
              <div class="category__information-container">
                <div class="category__information" data-aos="zoom-in-up">
                  <div class="category__information-wrapper"><a class="content__item content__item--category"
                      href="#"><img class="content__image content__image--category" src="<?php echo get_template_directory_uri() ?>/img/category__img1.jpg"
                        alt="category1"></a>
                    <div class="content__wrapper content__wrapper--category"><a class="category__link" href="#">
                        <div class="content__date content__date--category">
                          <p class="content__date-text content__date-text--category">22 Jun, 2020</p>
                        </div>
                        <div class="content__category-wrapper content__category-wrapper--small">
                          <div class="content__category-line">
                            <p class="category__description">南大沢中郷公園フリーマーケット</p>
                          </div>
                          <div class="content__title-line content__title-line--category">
                            <div class="category__description-container">
                              <p class="category__description category__description--small">開催日：2020年12月24日</p>
                              <p class="category__description category__description--small">開催場所：東京都 渋谷区</p>
                              <p class="category__description category__description--small">カテゴリ：フリーマーケット </p>
                            </div>
                          </div>
                        </div>
                      </a></div>
                  </div>
                </div>
                <div class="category__information" data-aos="zoom-in-up">
                  <div class="category__information-wrapper"><a class="content__item content__item--category"
                      href="#"><img class="content__image content__image--category" src="<?php echo get_template_directory_uri() ?>/img/category__img2.jpg"
                        alt="category2"></a>
                    <div class="content__wrapper content__wrapper--category"><a class="category__link" href="#">
                        <div class="content__date content__date--category">
                          <p class="content__date-text content__date-text--category">22 Jun, 2020</p>
                        </div>
                        <div class="content__category-wrapper content__category-wrapper--small">
                          <div class="content__category-line">
                            <p class="category__description">ぽっぽ町田蚤の市寺フェス2019秋の陣☆出店者・出演者募集☆残り後わずか♪</p>
                          </div>
                          <div class="content__title-line content__title-line--category">
                            <div class="category__description-container">
                              <p class="category__description category__description--small">開催日：2020年12月24日</p>
                              <p class="category__description category__description--small">開催場所：東京都 渋谷区</p>
                              <p class="category__description category__description--small">カテゴリ：フリーマーケット</p>
                            </div>
                          </div>
                          <div class="category__button-container"><a class="button" href="#">詳しく見る</a></div>
                        </div>
                      </a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="category__content">
            <div class="category__item-container"><a class="category__item" href="#">
                <div class="category__title-container" data-aos="flip-up">
                  <p class="category__title-jp category__title-jp--tawny">お知らせ</p>
                  <p class="category__title-en">INFO.</p>
                </div>
              </a></div>
            <div class="category__information-container" data-aos="zoom-in-up">
              <div class="category__information">
                <div class="category__information-wrapper"><a class="content__item content__item--category"
                    href="#"><img class="content__image content__image--category" src="<?php echo get_template_directory_uri() ?>/img/category__img3.jpg"
                      alt="category3"></a>
                  <div class="content__wrapper content__wrapper--category"><a class="category__link" href="#">
                      <div class="content__date content__date--category">
                        <p class="content__date-text content__date-text--category">22 Jun, 2020</p>
                      </div>
                      <div class="content__category-wrapper content__category-wrapper--small">
                        <div class="content__category-line">
                          <p class="category__description">南大沢中郷公園フリーマーケット</p>
                        </div>
                        <div class="content__title-line content__title-line--category">
                          <div class="category__description-container">
                            <p class="category__description category__description--small">開催日：2020年12月24日</p>
                            <p class="category__description category__description--small">開催場所：東京都 渋谷区</p>
                            <p class="category__description category__description--small">カテゴリ：フリーマーケット</p>
                          </div>
                        </div>
                        <div class="category__button-container"><a class="button" href="#">詳しく見る</a></div>
                      </div>
                    </a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="category__content">
            <div class="category__item-container"><a class="category__item" href="#">
                <div class="category__title-container" data-aos="flip-up">
                  <p class="category__title-jp category__title-jp--purple">コラム</p>
                  <p class="category__title-en">COLUMN</p>
                </div>
              </a></div>
            <div class="category__information-container" data-aos="zoom-in-up">
              <div class="category__information">
                <div class="category__information-wrapper"><a class="content__item content__item--category"
                    href="#"><img class="content__image content__image--category" src="<?php echo get_template_directory_uri() ?>/img/category__img4.jpg"
                      alt="category4"></a>
                  <div class="content__wrapper content__wrapper--category"><a class="category__link" href="#">
                      <div class="content__date content__date--category">
                        <p class="content__date-text content__date-text--category">22 Jun, 2020</p>
                      </div>
                      <div class="content__category-wrapper content__category-wrapper--small">
                        <div class="content__category-line">
                          <p class="category__description">南大沢中郷公園フリーマーケット</p>
                        </div>
                        <div class="content__title-line content__title-line--category">
                          <div class="category__description-container">
                            <p class="category__description category__description--small">開催日：2020年12月24日</p>
                            <p class="category__description category__description--small">開催場所：東京都 渋谷区</p>
                            <p class="category__description category__description--small">カテゴリ：フリーマーケット</p>
                          </div>
                        </div>
                        <div class="category__button-container"><a class="button" href="#">詳しく見る</a></div>
                      </div>
                    </a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <section class="banner" data-aos="zoom-in">
      <div class="banner__container banner__container--second"><img class="banner__image" src="<?php echo get_template_directory_uri() ?>/img/banner__img2.jpg"
          alt="Banner 2"><img class="banner__image banner__image--sp" src="<?php echo get_template_directory_uri() ?>/img/banner__img2-sp.jpg" alt="Banner 2">
        <div class="banner__wrapper">
          <div class="banner__arc-container"><img class="banner__arc" src="<?php echo get_template_directory_uri() ?>/img/arc.png" alt="Arc"></div>
          <div class="banner__text-container banner__text-container--second">
            <p class="banner__text banner__text--extra-small">ファンタ</p>
            <h1 class="banner__text banner__text--title">Funta</h1>
            <h1 class="banner__text">ファンワッカの<br class="banner__break">イベントスタッフ募集中</h1>
            <p class="banner__text banner__text--description">これはダミー文字です新型コロナウイルス緊急事態宣言解除後の当社対応についてのお知らせ<br
                class="banner__break">染まってないからこそ、新たな風を吹き込めるリアルをコアとした体験を</p>
          </div>
          <div class="banner__button-container banner__button-container--second"><a class="button" href="#">募集について</a>
          </div>
        </div>
      </div>
    </section>
    <div class="content">
      <section class="plan">
        <div class="container__content">
          <div class="plan__container">
            <div class="plan__title-container">
              <div class="title" data-aos="zoom-in">
                <div class="title__lines title__lines--category">
                  <h1 class="title__text">PLAN</h1>
                </div>
              </div>
              <div class="plan__business-container">
                <div class="plan__business-box" data-aos="zoom-in-right"><img class="plan__image"
                    src="<?php echo get_template_directory_uri() ?>/img/plan__img1.jpg" alt="Business Plan Image 1">
                  <div class="plan__text-container">
                    <div class="plan__business-text">法人向け事業</div>
                    <div class="plan__business-description">キャッチコピーが⼊りますリアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証</div>
                    <div class="plan__business-description--sp">キャッチコピーが⼊りますリアルをコアとした体験をデザインし、情報拡散や態度変容な...効果を</div>
                    <div class="plan__button-container"><a class="button" href="#">詳しく見る</a></div>
                  </div>
                </div>
                <div class="plan__business-box" data-aos="zoom-in-right"><img class="plan__image"
                    src="<?php echo get_template_directory_uri() ?>/img/plan__img2.jpg" alt="Business Plan Image 2">
                  <div class="plan__text-container">
                    <div class="plan__business-text plan__business-text--caribbeangreen">公共向け事業</div>
                    <div class="plan__business-description">キャッチコピーが⼊りますリアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証</div>
                    <div class="plan__business-description--sp">キャッチコピーが⼊りますリアルをコアとした体験をデザインし、情報拡散や態度変容な...効果を</div>
                    <div class="plan__button-container"><a class="button" href="#">詳しく見る</a></div>
                  </div>
                </div>
                <div class="plan__business-box" data-aos="zoom-in-right"><img class="plan__image"
                    src="<?php echo get_template_directory_uri() ?>/img/plan__img3.jpg" alt="Business Plan Image 3">
                  <div class="plan__text-container">
                    <div class="plan__business-text plan__business-text--tawny">地域活性化事業</div>
                    <div class="plan__business-description">キャッチコピーが⼊りますリアルをコアとした体験をデザインし、情報拡散や態度変容などの効果を検証</div>
                    <div class="plan__business-description--sp">キャッチコピーが⼊りますリアルをコアとした体験をデザインし、情報拡散や態度変容な...効果を</div>
                    <div class="plan__button-container"><a class="button" href="#">詳しく見る</a></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="plan__instagram-container" data-aos="zoom-in-left"><img class="plan__instagram-page"
                src="<?php echo get_template_directory_uri() ?>/img/funwacca__ig.png" alt="Instagram Page of Funwacca"></div>
          </div>
        </div>
      </section>
    </div>
    <div class="content">
      <section class="pickup">
        <div class="container__content">
          <div class="title" data-aos="zoom-in">
            <div class="title__lines">
              <h1 class="title__text">EVENT</h1>
            </div>
          </div>
          <div class="content__item-container content__item-container--event"><a class="content__item" href="#"
              data-aos="flip-left"><img class="content__image content__image--event" src="<?php echo get_template_directory_uri() ?>/img/event__img1.jpg"
                alt="event1">
              <div class="content__wrapper content__wrapper--event">
                <div class="content__category-wrapper content__category-wrapper--event">
                  <div class="content__category-line content__category-line--event">
                    <p class="content__text-event">さまざまな業種や形態の体験施策に携わってきた経験値を武器に難な状 況を乗り越える</p>
                  </div>
                  <div class="event__cont">
                    <div class="content__date content__date--event">
                      <p class="content__date-text">22 Jun, 2020</p>
                    </div>
                  </div>
                  <div class="content__title-line content__title-line--event">
                    <div class="event__description-container">
                      <p class="event__description">開催日：2020年12月24日</p>
                      <p class="event__description">開催場所：東京都 渋谷区</p>
                      <p class="event__description">カテゴリ：フリーマーケット</p>
                    </div>
                  </div>
                </div>
              </div>
            </a><a class="content__item" href="#" data-aos="flip-left"><img class="content__image content__image--event"
                src="<?php echo get_template_directory_uri() ?>/img/event__img2.jpg" alt="event2">
              <div class="content__wrapper content__wrapper--event">
                <div class="content__category-wrapper content__category-wrapper--event">
                  <div class="content__category-line content__category-line--event">
                    <p class="content__text-event">イベント名染まってないからこそ、新たな風を吹き込める</p>
                  </div>
                  <div class="event__cont">
                    <div class="content__date content__date--event">
                      <p class="content__date-text">22 Jun, 2020</p>
                    </div>
                  </div>
                  <div class="content__title-line content__title-line--event">
                    <div class="event__description-container">
                      <p class="event__description">開催日：2020年12月24日</p>
                      <p class="event__description">開催場所：東京都 渋谷区</p>
                      <p class="event__description">カテゴリ：フリーマーケット</p>
                    </div>
                  </div>
                </div>
              </div>
            </a><a class="content__item" href="#" data-aos="flip-left"><img class="content__image content__image--event"
                src="<?php echo get_template_directory_uri() ?>/img/event__img3.jpg" alt="event3">
              <div class="content__wrapper content__wrapper--event">
                <div class="content__category-wrapper content__category-wrapper--event">
                  <div class="content__category-line content__category-line--event">
                    <p class="content__text-event">宮津満腹祭 うまいもん集まれ 2020年12月24日開催</p>
                  </div>
                  <div class="event__cont">
                    <div class="content__date content__date--event">
                      <p class="content__date-text">22 Jun, 2020</p>
                    </div>
                  </div>
                  <div class="content__title-line content__title-line--event">
                    <div class="event__description-container">
                      <p class="event__description">開催日：2020年12月24日</p>
                      <p class="event__description">開催場所：東京都 渋谷区</p>
                      <p class="event__description">カテゴリ：フリーマーケット</p>
                    </div>
                  </div>
                </div>
              </div>
            </a></div>
        </div>
      </section>
    </div>
  </div>
<?php get_footer() ?>