<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=11">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
  <link
    href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@100;300;400;500;700;900&amp;family=Noto+Serif+JP:ital,wght@0,200;0,400;0,700;1,400;1,700&amp;display=swap"
    rel="stylesheet">
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans&amp;display=swap" rel="stylesheet">
  <link href="https://unpkg.com/flickity@2/dist/flickity.min.css" type="text/css" rel="stylesheet">
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
  <title>Funwacca</title>
  <link href="<?php echo get_template_directory_uri() ?>/style.css" rel="stylesheet">
  <?php wp_head() ?>
</head>

<body>
  <nav class="header" data-aos="fade-down">
    <div class="header__container">
      <div class="header__line"><img class="header__line" src="<?php echo get_template_directory_uri() ?>/img/header_logo.png" alt="logo"></div>
      <div class="header__callout-container">
        <div class="header__callout"><img src="<?php echo get_template_directory_uri() ?>/img/callout.png" alt="callout">
          <p class="header__callout-text">イベント・企画づくりを応援するメディア</p>
        </div>
      </div>
    </div>
    <div class="header__container">
      <div class="header__search"> <input class="header__search-box" type="text" placeholder="検索">
        <div class="header__search-container"><button class="header__search-btn" type="button"><img
              src="<?php echo get_template_directory_uri() ?>/img/search_btn.png" alt="logo"></button></div>
      </div>
      <div class="header__operation-container"><a class="header__operation" href="#">運営会社</a></div>
      <div class="header__contact-container"><a class="header__contact" href="/contact.html">お問い合わせ</a></div>
    </div>
  </nav>
  <header class="nav">
    <div class="nav__logo"><a href="#"></a><img class="nav__logo" src="<?php echo get_template_directory_uri() ?>/img/header_logo.png" alt="logo"></div>
    <div class="nav__icon-container"><input class="nav__btn" id="nav__btn" type="checkbox"><label class="nav__icon"
        for="nav__btn"><span class="nav__icon--line"></span></label>
      <ul class="nav__links" id="nav__links">
        <li class="nav__links--white">
          <div class="nav__search"> <input class="nav__search-box" type="text" placeholder="検索"><button
              class="nav__search-btn" type="button"><img src="<?php echo get_template_directory_uri() ?>/img/search_btn.png" alt="search btn"></button></div>
        </li>
        <li class="nav__links--link"><a class="nav__item" href="#">お知らせ</a></li>
        <li class="nav__links--link"><label class="nav__link-container" for="dropdown"><input class="nav__dropdownchk"
              id="dropdown" type="checkbox"><a class="nav__item--dropdown">カテゴリ<div class="nav__dropdown-toggle">
                <div class="nav__arrow-toggle"></div>
              </div>
              <div class="nav__dropdown-hover">
                <div class="nav__arrow-hover"></div>
              </div>
            </a>
            <div class="nav__box nav__box--arrow">
              <ul class="nav__dropdown">
                <li class="nav__text"><span class="nav__dropdown-arrow"></span><a class="nav__dropdown-text"
                    href="#">イベント</a></li>
                <li class="nav__text"><span class="nav__dropdown-arrow"></span><a class="nav__dropdown-text"
                    href="#">お知らせ</a></li>
                <li class="nav__text"><span class="nav__dropdown-arrow"></span><a class="nav__dropdown-text"
                    href="#">募集</a></li>
                <li class="nav__text"><span class="nav__dropdown-arrow"></span><a class="nav__dropdown-text"
                    href="#">講習会</a></li>
                <li class="nav__text"><span class="nav__dropdown-arrow"></span><a class="nav__dropdown-text"
                    href="#">コラム</a></li>
                <li class="nav__text"><span class="nav__dropdown-arrow"></span><a class="nav__dropdown-text"
                    href="#">ワークショップ</a></li>
              </ul>
            </div>
          </label></li>
        <li class="nav__links--link"><a class="nav__item" href="#">イベント情報</a></li>
        <li class="nav__links--link"><a class="nav__item" href="#">イベントスタッフ募集中！</a></li>
        <li class="nav__links--link"><a class="nav__item" href="#">このサイトについて</a></li>
        <li class="nav__links--link"><a class="nav__item" href="#">イベント企画‧まちづくりの⽅へ</a></li>
        <li class="nav__links--white"><a class="nav__item--black" href="#">運営会社</a></li>
        <li class="nav__links--contact">
          <div class="nav__contact-container"><a class="nav__contact" href="<?php echo get_home_url() ?>">お問い合わせ</a></div>
        </li>
      </ul>
    </div>
  </header>