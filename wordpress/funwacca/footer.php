<section class="footer">
    <div class="footer__container">
      <div class="footer__contact" data-aos="flip-up"><a class="footer__contact-text" href="#">お問い合わせ</a></div>
      <div class="footer__link" data-aos="fade-right"><a class="footer__navigation" href="#">お知らせ</a><a
          class="footer__navigation" href="#">カテゴリ</a><a class="footer__navigation" href="#">イベント情報</a><a
          class="footer__navigation" href="#">イベントスタッフ募集中！</a><a class="footer__navigation" href="#">このサイトについて</a><a
          class="footer__navigation" href="#">イベント企画‧まちづくりの⽅へ</a><a class="footer__navigation" href="#">運営会社</a><a
          class="footer__navigation" href="#">個人情報の取り扱いについて</a><a class="footer__navigation" href="#">サイトマップ</a></div>
      <div class="footer__icon-container" data-aos="flip-left"><a class="footer__icon" href="#"><img
            src="<?php echo get_template_directory_uri() ?>/img/facebook-icon.png" alt="Facebook"></a><a class="footer__icon" href="#"><img
            src="<?php echo get_template_directory_uri() ?>/img/twitter-icon.png" alt="Twitter"></a><a class="footer__icon" href="#"><img
            src="<?php echo get_template_directory_uri() ?>/img/instagram-icon.png" alt="Instagram"></a></div>
      <div class="footer__logo-container"><a href="#"><img class="footer__logo" src="<?php echo get_template_directory_uri() ?>/img/footer__logo.png"
            alt="Funwacca Logo"></a></div>
      <p class="footer__copyright">Copyright © 2020 Funwacca</p>
      <div class="footer__top"><img class="footer__arrow" src="<?php echo get_template_directory_uri() ?>/img/arrow_top.png" alt="Arrow up"></div>
    </div>
  </section>
  <script src="<?php echo get_template_directory_uri() ?>/js/ofi.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/smoothscroll-polyfill@0.4.4/dist/smoothscroll.min.js"></script>
  <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/main.js"></script>
  <?php wp_footer() ?>
</body>

</html>